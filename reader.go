// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

package ini

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"reflect"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

// ErrUnknownRecord is returned from Read() when the record name in []
// did not match any registered name.  Use the ErrorRecordName()
// method to recover the name in [].
var ErrUnknownRecord = fmt.Errorf("Unknown record type in INI file")

// ErrUnknownField is returned from Read() when a field name is unknown
// (does not exist in the struct).  Use the ErrorFieldName() method to
// recover the field name.
var ErrUnknownField = fmt.Errorf("Unknown field name in INI file")

// ErrParse is returned from Read() when some part of the INI file
// could not be parsed successfully.  Use the ErrorParseInfo() method
// to get more detail about the parsing problem.
var ErrParse = fmt.Errorf("Parse error in INI file")

// Reader implements a parser on top of an io.Reader, parsing whole
// records with an INI-style syntax from the file.
type Reader struct {
	file *bufio.Reader
	eof  bool

	reg registry

	lax_records bool
	lax_fields  bool

	// the current line and next line, if any.
	line *[]byte
	next *[]byte

	// line numbers
	raw_lno  int
	line_lno int
	next_lno int

	cur_record string
	cur_field  string
	parse_msg  string

	// last Read() aborted in the middle of a record, so we
	// need to seek to the next record.
	recovering bool
}

// Callback is a function which is called by Read() when it has
// detected and parsed a non-singleton record in the INI file.
// It supplies the new object: a pointer to a Go struct, the full
// name of the record (which was in square brackets), as well as
// the suffix of the name (i.e. the part matching the wildcard).
//
// If the struct has a field called "Id", then it is automatically
// set to the matched suffix string.
//
// You should type assert the 'rec_ptr' value to the type which
// was registered with RegisterType.  This type assertion should
// never fail.
type Callback func(rec_ptr interface{}, name, suffix string)

type registry struct {
	list   []*knownType
	sorted bool
}

type knownType struct {
	match  string
	is_var bool

	// this is the struct itself (NOT the pointer to it)
	value reflect.Value

	callback Callback
}

// NewReader creates a Reader which reads from the given io.Reader.
// This is the only way to create a valid Reader object.  While using
// the Reader, the underlying object (a file, etc) should not be used
// directly.
func NewReader(file io.Reader) *Reader {
	r := new(Reader)
	r.reg.init()
	r.file = bufio.NewReader(file)
	return r
}

// RegisterVar registers a variable of a struct type with a Reader.
// This represents a singleton record in the INI file, with a name
// which must match exactly.  If such a record is found in the file,
// it is updated by the field values.  There is no way to determine
// if the record did actually exist, and there is no check for the
// record appearing more than once.
//
// The 'rec_ptr' parameter is a pointer to the variable.
func (r *Reader) RegisterVar(name string, rec_ptr interface{}) {
	if rec_ptr == nil {
		panic("nil value given to ini.RegisterVar")
	}

	ptr := reflect.ValueOf(rec_ptr)

	if ptr.Type() == nil || ptr.Kind() != reflect.Ptr {
		panic("non-pointer value given to ini.RegisterVar")
	}
	if ptr.IsNil() {
		panic("nil pointer given to ini.RegisterVar")
	}

	rec := ptr.Elem()

	if rec.Kind() != reflect.Struct {
		panic("pointer to non-struct given to ini.RegisterVar")
	}

	if name == "" {
		panic("empty name given to ini.RegisterVar")
	}
	if strings.Contains(name, "*") {
		panic("wildcard name given to ini.RegisterVar")
	}

	name = strings.ToUpper(name)

	r.reg.addVar(name, rec)
}

// RegisterType registers a struct type with a Reader.  It includes a
// match string used to detect records using this type, by matching
// against the name in [], and a callback function which is called
// when a record of this type is detected and successfully parsed.
//
// The match string must be a prefix followed by an asterix '*' which
// represents a wildcard.  The prefix may be empty, allowing all names
// to match the wildcard.  An asterix is not valid in any position
// except the end.  If more than one record type can be used in a file,
// it is recommended they all have unique prefixes to distinguish them.
// Prefixes should have an obvious separator before the '*', for
// example: "FOO_*", "FOO/*", or "FOO:*" would all work well.
//
// Note: when matching, singletons registered with RegisterVar()
// always take precedence over any types registered via this method.
//
// The 'rec_ptr' parameter must be a pointer to a variable (etc) of
// the data structure to be registered.  It is only used to establish
// type information, the variable itself is not read or modified.
func (r *Reader) RegisterType(match string, rec_ptr interface{},
	callback Callback) {

	if rec_ptr == nil {
		panic("nil value given to ini.RegisterType")
	}

	ptr := reflect.ValueOf(rec_ptr)

	if ptr.Type() == nil || ptr.Kind() != reflect.Ptr {
		panic("non-pointer value given to ini.RegisterType")
	}
	if ptr.IsNil() {
		panic("nil pointer given to ini.RegisterType")
	}

	rec := ptr.Elem()

	if rec.Kind() != reflect.Struct {
		panic("pointer to non-struct given to ini.RegisterType")
	}

	if match == "" {
		panic("empty match given to ini.RegisterType")
	}
	if !strings.HasSuffix(match, "*") {
		panic("bad match given to ini.RegisterType (no wildcard)")
	}

	match = strings.ToUpper(match)

	r.reg.addType(match, rec, callback)
}

// EnableLaxRecords causes the reader to ignore records whose names
// did not match any registered vars or types.  The default behavior
// is to return ErrUnknownRecord when this occurs.
func (r *Reader) EnableLaxRecords() {
	r.lax_records = true
}

// EnableLaxFields causes the reader to ignore unknown fields within
// a record.  The default behavior is to return the ErrUnknownField
// error, which aborts the parsing of the current record.
func (r *Reader) EnableLaxFields() {
	r.lax_fields = true
}

// ErrorLineNum returns the line number within the INI file where an
// error occurred (from the Read method), especially the custom errors
// defined in this package.  Line numbers begin at 1.
func (r *Reader) ErrorLineNum() int {
	if r.line_lno > 0 {
		return r.line_lno
	} else {
		return 1
	}
}

// ErrorRecordName returns the name of the current record after an
// error is returned from Read().  For ErrUnknownRecord, it is the name
// of the record which did not match any registered vars or types.
func (r *Reader) ErrorRecordName() string {
	return r.cur_record
}

// ErrorFieldName returns the name of the current field after an
// error is returned from Read().  For ErrUnknownField, it is the
// name of the unknown field itself.
func (r *Reader) ErrorFieldName() string {
	return r.cur_field
}

// ErrorParseInfo returns a string describing a parsing error, and
// can only be used after Read() returns ErrParse.
func (r *Reader) ErrorParseInfo() string {
	return r.parse_msg
}

func (reg *registry) init() {
	reg.list = make([]*knownType, 0, 4)
}

func (reg *registry) addVar(name string, rec reflect.Value) {
	kt := knownType{match: name, is_var: true, value: rec}

	reg.list = append(reg.list, &kt)
	reg.sorted = false
}

func (reg *registry) addType(match string, rec reflect.Value, callback Callback) {
	// remove the trailing '*' from the match
	if match[len(match)-1] == '*' {
		match = match[0 : len(match)-1]
	}

	kt := knownType{match: match, is_var: false, value: rec, callback: callback}

	reg.list = append(reg.list, &kt)
	reg.sorted = false
}

func (reg *registry) sort() {
	sort.Slice(reg.list,
		func(i, k int) bool {
			kt1 := reg.list[i]
			kt2 := reg.list[k]

			// singleton vars must be before wildcard types
			if kt1.is_var != kt2.is_var {
				return kt1.is_var
			}

			// longer matches should be before shorter ones
			return len(kt1.match) > len(kt2.match)
		})

	reg.sorted = true
}

func (reg *registry) find(name string) (kt *knownType, suffix string) {
	// ensure match names are sorted from longest to shortest
	if !reg.sorted {
		reg.sort()
	}

	// we need to match case insensitively, so the caller is required
	// to make the name uppercase.

	for _, kt = range reg.list {
		if kt.is_var {
			// a singleton var?
			if name == kt.match {
				return
			}
		} else {
			if kt.match == "" {
				// a "*" matches everything
				suffix = name
				return
			}
			if len(name) > len(kt.match) && strings.HasPrefix(name, kt.match) {
				suffix = strings.TrimPrefix(name, kt.match)
				return
			}
		}
	}

	// not found
	kt = nil
	return
}

func (r *Reader) setId(rec reflect.Value, id string) {
	found, index := r.findField(rec, "Id")

	if found {
		field := rec.Field(index)

		field.SetString(id)
	}
}

// ReadAll calls the Read() method repeatedly in order to process all
// the records in the INI file.   If any error occurs, it is returned
// immediately.  This method is for convenience; you have more control
// over errors by calling Read() directly.
//
// Use EnableLaxRecords() to ignore unrecognized record names, and
// EnableLaxFields() to ignore unknown field names.  Parsing and file
// errors are serious and should never be ignored.
func (r *Reader) ReadAll() error {
	for {
		if err := r.Read(); err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}
	}
}

// Read parses the next record from the INI file.  If the name in []
// is matched against a previously registered type, then an object of
// that type is constructed, the fields are parsed, and the associated
// callback function is called with a pointer to the new object.
//
// Note: unsupported types in the struct will cause a panic.
//
// If the record name could not be matched, the ErrUnknownRecord error
// is returned.  If a field name does not exist in the struct, then
// parsing the current record is aborted and ErrUnknownField is
// returned (unless EnableLaxFields has been called).  If something
// in the INI file failed to parse, then ErrParse is returned.
// Later records may still be read via Read() after these errors.
//
// If no more records were found in the file, then io.EOF is returned.
// All other errors will be I/O errors from the io.Reader, and hence
// they should be treated as fatal / unrecoverable.  The behavior of
// Read() after a file error is undefined.
//
// On success, nil is returned.
func (r *Reader) Read() error {
	r.cur_record = ""
	r.cur_field = ""
	r.parse_msg = ""

	// if we have a line from a previous Read(), use it
	if r.line == nil {
		if err := r.nextLine(); err != nil {
			return err
		}
	}

	// Note: nextLine() ensures the line is not empty
	line := r.consumeLine()

	// last Read() was aborted, so seek to next record
	if r.recovering {
		for line[0] != '[' {
			if err := r.nextLine(); err != nil {
				return err
			}
			line = r.consumeLine()
		}
	}

	if line[0] != '[' {
		r.parse_msg = "missing '[' for start of record"
		return ErrParse
	}

	closer := bytes.IndexByte(line, ']')
	if closer < 0 {
		r.parse_msg = "missing ']' for record name"
		r.recovering = true
		return ErrParse
	}

	name := string(line[1:closer])

	if len(name) == 0 {
		r.parse_msg = "empty record name in []"
		r.recovering = true
		return ErrParse
	}

	// uppercasing the name is required for registry.find() to work
	name = strings.ToUpper(name)

	r.cur_record = name

	kt, suffix := r.reg.find(name)
	if kt == nil {
		r.recovering = true

		if r.lax_records {
			return nil
		}
		return ErrUnknownRecord
	}

	// create a new object of the user's type
	var ptr reflect.Value
	var rec reflect.Value

	if kt.is_var {
		rec = kt.value
		// ptr is not used
	} else {
		ptr = reflect.New(kt.value.Type())
		rec = ptr.Elem()
	}

	/* parse fields.... */

	for {
		// only get file errors here
		err := r.nextLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			r.eof = true
			return err
		}

		// check for start of a new record
		if (*r.line)[0] == '[' {
			// keep this line for the next Read()
			break
		}

		line := r.consumeLine()

		// only get parse errors here, and ErrUnknownField
		err = r.parseField(rec, line)
		if err != nil {
			r.recovering = true
			return err
		}
	}

	// pass the object to the associated callback
	if kt.callback != nil {
		r.setId(rec, suffix)

		kt.callback(ptr.Interface(), name, suffix)
	}

	return nil // success
}

func (r *Reader) parseField(rec reflect.Value, line []byte) error {
	if isExtensionLine(line) {
		r.parse_msg = "unexpected extension line"
		return ErrParse
	}

	equals := bytes.IndexByte(line, '=')
	if equals < 0 {
		r.parse_msg = "missing '=' on field line"
		return ErrParse
	}

	// check the equal sign was not inside a string
	if bytes.IndexByte(line[0:equals], '"') >= 0 {
		r.parse_msg = "unexpected string on field line"
		return ErrParse
	}

	name := string(bytes.TrimSpace(line[0:equals]))
	if name == "" || !isKeyword(name) || strings.EqualFold(name, "id") {
		r.parse_msg = "invalid field name: '" + name + "'"
		return ErrParse
	}

	r.cur_field = name

	// skip whitespace after equals sign
	line = bytes.TrimSpace(line[equals+1:])

	if len(line) == 0 {
		r.parse_msg = "missing value on field line"
		return ErrParse
	}

	// find the field in the user's struct
	found, index := r.findField(rec, name)

	if !found {
		if r.lax_fields {
			return nil
		}
		return ErrUnknownField
	}

	field := rec.Field(index)
	info := rec.Type().Field(index)
	kind := field.Kind()

	if !field.CanSet() {
		panic("non-writable struct field: '" + info.Name + "'")
	}

	// all parsing from here on is done on a bytes.Buffer
	buf := bytes.NewBuffer(line)

	// handle "primitive" types
	if kind < reflect.Array {
		return r.parsePrimitive(field, buf, true)
	}

	switch kind {
	case reflect.String:
		return r.parseString(field, buf)

	case reflect.Slice:
		return r.parseArray(field, buf)

	case reflect.Struct:
		return r.parseStruct(field, buf)

	case reflect.Map:
		return r.parseMap(field, buf)

	default:
		panic("unsupported field type: " + kind.String())
	}
}

func (r *Reader) findField(rec reflect.Value, name string) (bool, int) {
	tp := rec.Type()

	for i := 0; i < rec.NumField(); i++ {
		info := tp.Field(i)

		// ignore non-exported fields
		if info.PkgPath != "" {
			continue
		}

		// case-insensitive comparison
		if strings.EqualFold(name, info.Name) {
			return true, i
		}
	}

	return false, -1
}

func (r *Reader) parsePrimitive(field reflect.Value, buf *bytes.Buffer,
	top_level bool) error {

	kind := field.Kind()

	switch kind {
	// a "tuple struct" is considered a primitive too
	case reflect.Struct:
		if err := r.parseTuple(field, buf, false); err != nil {
			return err
		}

	case reflect.String:
		temp := r.parseKeyword(buf)
		if temp == "" {
			if _, err := fmt.Fscanf(buf, "%q", &temp); err != nil {
				r.parse_msg = err.Error()
				return ErrParse
			}
		}
		field.SetString(temp)

	case reflect.Bool:
		// this is more strict that using fmt.Sscanf() or similar
		temp := r.parseKeyword(buf)
		temp = strings.ToLower(temp)
		switch temp {
		case "false":
			field.SetBool(false)
		case "true":
			field.SetBool(true)
		default:
			r.parse_msg = "bad boolean value"
			return ErrParse
		}

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		var temp int64
		if _, err := fmt.Fscanf(buf, "%v", &temp); err != nil {
			r.parse_msg = err.Error()
			return ErrParse
		}
		field.SetInt(temp)

		if expectSymbol(buf, '.') {
			r.parse_msg = "floating point where integer expected"
			return ErrParse
		}

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		var temp uint64
		if _, err := fmt.Fscanf(buf, "%v", &temp); err != nil {
			r.parse_msg = err.Error()
			return ErrParse
		}
		field.SetUint(temp)

		if expectSymbol(buf, '.') {
			r.parse_msg = "floating point where integer expected"
			return ErrParse
		}

	case reflect.Float32, reflect.Float64:
		var temp float64
		if _, err := fmt.Fscanf(buf, "%v", &temp); err != nil {
			r.parse_msg = err.Error()
			return ErrParse
		}
		field.SetFloat(temp)

	case reflect.Complex64, reflect.Complex128:
		var temp complex128
		if _, err := fmt.Fscanf(buf, "%v", &temp); err != nil {
			r.parse_msg = err.Error()
			return ErrParse
		}
		field.SetComplex(temp)

	default:
		panic("composite types cannot be nested")
	}

	if top_level {
		// check there are no extra characters
		if buf.Len() > 0 {
			r.parse_msg = "bad field value (extra characters)"
			return ErrParse
		}
	}

	return nil // OK
}

func (r *Reader) parseString(field reflect.Value, buf *bytes.Buffer) error {
	// handle a plain keyword
	ident := r.parseKeyword(buf)
	if ident != "" {
		// ensure no other characters after the identifier
		if buf.Len() > 0 {
			r.parse_msg = "bad keyword (extra characters)"
			return ErrParse
		}

		field.SetString(ident)
		return nil
	}

	// handle multiple quoted strings joined by a "+" operator

	field.SetString("")

	for {
		// parse a single fragment
		var temp string

		if _, err := fmt.Fscanf(buf, "%q", &temp); err != nil {
			if err == io.EOF {
				break
			}
			r.parse_msg = err.Error()
			return ErrParse
		}

		field.SetString(field.String() + temp)

		// check for '+', but ignoring whitespace
		skipWhitespace(buf)

		if buf.Len() == 0 {
			break
		}

		if !expectSymbol(buf, '+') {
			r.parse_msg = "expected '+' between strings"
			return ErrParse
		}

		skipWhitespace(buf)

		if buf.Len() == 0 {
			r.parse_msg = "missing string after '+'"
			return ErrParse
		}
	}

	return nil // OK
}

func (r *Reader) parseArray(field reflect.Value, buf *bytes.Buffer) error {
	// Note: field must be a slice, fixed-size arrays are not supported.

	if !expectSymbol(buf, '[') {
		r.parse_msg = "expected '[' to begin array data"
		return ErrParse
	}

	elem_type := field.Type().Elem()

	// create a slice value
	slice := reflect.MakeSlice(field.Type(), 0, 10)

	for {
		skipWhitespace(buf)

		if expectSymbol(buf, ']') {
			break
		}

		if buf.Len() == 0 {
			r.parse_msg = "missing value for array"
			return ErrParse
		}

		value := reflect.New(elem_type).Elem()

		if err := r.parsePrimitive(value, buf, false); err != nil {
			return err
		}

		slice = reflect.Append(slice, value)

		skipWhitespace(buf)

		if expectSymbol(buf, ']') {
			break
		}
		if !expectSymbol(buf, ',') {
			r.parse_msg = "expected ',' between array values"
			return ErrParse
		}
	}

	field.Set(slice)
	return nil // OK
}

func (r *Reader) parseStruct(field reflect.Value, buf *bytes.Buffer) error {
	// handle the tuple syntax
	if expectSymbol(buf, '(') {
		return r.parseTuple(field, buf, true)
	}

	if !expectSymbol(buf, '{') {
		r.parse_msg = "expected '{' to begin struct data"
		return ErrParse
	}

	for {
		skipWhitespace(buf)

		if expectSymbol(buf, '}') {
			break
		}

		if buf.Len() == 0 {
			r.parse_msg = "missing value for struct"
			return ErrParse
		}

		key := r.parseKeyword(buf)
		if key == "" {
			r.parse_msg = "expected field name for struct"
			return ErrParse
		}
		skipWhitespace(buf)

		if !(expectSymbol(buf, ':') || expectSymbol(buf, '=')) {
			r.parse_msg = "missing ':' between field name and value"
			return ErrParse
		}
		skipWhitespace(buf)

		found, index := r.findField(field, key)

		if !found {
			r.parse_msg = "unknown field in sub-struct: " + key
			return ErrParse
		}

		if err := r.parsePrimitive(field.Field(index), buf, false); err != nil {
			return err
		}
		skipWhitespace(buf)

		if expectSymbol(buf, '}') {
			break
		}
		if !expectSymbol(buf, ',') {
			r.parse_msg = "expected ',' between struct values"
			return ErrParse
		}
	}

	return nil // OK
}

func (r *Reader) parseTuple(field reflect.Value, buf *bytes.Buffer,
	seen_parenthesis bool) error {

	if !seen_parenthesis && !expectSymbol(buf, '(') {
		r.parse_msg = "expected '(' to begin tuple"
		return ErrParse
	}

	tp := field.Type()

	// need to count the exported fields
	total := 0
	for i := 0; i < tp.NumField(); i++ {
		if tp.Field(i).PkgPath == "" {
			total++
		}
	}

	seen := 0

	for i := 0; i < tp.NumField(); i++ {
		info := tp.Field(i)

		// ignore non-exported fields
		if info.PkgPath != "" {
			continue
		}

		skipWhitespace(buf)

		if buf.Len() == 0 || expectSymbol(buf, ')') {
			r.parse_msg = "missing value for tuple"
			return ErrParse
		}

		if err := r.parsePrimitive(field.Field(i), buf, false); err != nil {
			return err
		}
		skipWhitespace(buf)

		seen++

		if seen < total {
			if !expectSymbol(buf, ',') {
				r.parse_msg = "expected ',' between tuple values"
				return ErrParse
			}
			skipWhitespace(buf)
		}
	}

	if !expectSymbol(buf, ')') {
		r.parse_msg = "missing ')' to end tuple"
		return ErrParse
	}

	return nil // OK
}

func (r *Reader) parseMap(field reflect.Value, buf *bytes.Buffer) error {
	if !expectSymbol(buf, '{') {
		r.parse_msg = "expected '{' to begin map data"
		return ErrParse
	}

	key_type := field.Type().Key()
	elem_type := field.Type().Elem()

	// create an empty map value
	m := reflect.MakeMap(field.Type())

	for {
		skipWhitespace(buf)

		if expectSymbol(buf, '}') {
			break
		}

		if buf.Len() == 0 {
			r.parse_msg = "missing value for map"
			return ErrParse
		}

		key := reflect.New(key_type).Elem()

		if err := r.parsePrimitive(key, buf, false); err != nil {
			return err
		}
		skipWhitespace(buf)

		if !(expectSymbol(buf, ':') || expectSymbol(buf, '=')) {
			r.parse_msg = "missing ':' between map key and value"
			return ErrParse
		}
		skipWhitespace(buf)

		value := reflect.New(elem_type).Elem()

		if err := r.parsePrimitive(value, buf, false); err != nil {
			return err
		}
		skipWhitespace(buf)

		m.SetMapIndex(key, value)

		if expectSymbol(buf, '}') {
			break
		}
		if !expectSymbol(buf, ',') {
			r.parse_msg = "expected ',' between map values"
			return ErrParse
		}
	}

	field.Set(m)
	return nil // OK
}

func (r *Reader) parseKeyword(buf *bytes.Buffer) string {
	ch, _, err := buf.ReadRune()
	if err != nil {
		return ""
	}
	if !(unicode.IsLetter(ch) || ch == '_') {
		buf.UnreadRune()
		return ""
	}

	runes := make([]rune, 0, 40)

	for {
		runes = append(runes, ch)

		ch, _, err = buf.ReadRune()
		if err != nil {
			break
		}
		if !(unicode.IsLetter(ch) || unicode.IsDigit(ch) || ch == '_') {
			buf.UnreadRune()
			break
		}
	}

	return string(runes)
}

// bring in the next line from the INI file, handling extension lines,
// and skipping blank lines and comments.  Returns io.EOF at end of
// the file.
func (r *Reader) nextLine() error {
	for {
		// pop the current line
		r.line = r.next
		r.line_lno = r.next_lno
		r.next = nil

		if r.line == nil {
			line, err := r.rawReadLine()

			// an EOF here should be returned to the caller
			if err != nil {
				return err
			}

			r.line = &line
			r.line_lno = r.raw_lno
		}

		if !isBlankOrComment(*r.line) {
			break
		}
	}

	// Ok we have a line with something on it.
	// now handle extension lines....

	for {
		next, err := r.rawReadLine()

		// an EOF here should NOT be returned, as we have a current
		// line which the caller can use.
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if !isExtensionLine(next) {
			r.next = &next
			r.next_lno = r.raw_lno
			break
		}

		r.applyExtension(next)
	}

	return nil // OK
}

func (r *Reader) applyExtension(next []byte) {
	// Note: it is not necessary to remove the leading whitespace
	// (the only advantage would be saving a bit of memory).

	size := len(*r.line)
	temp := make([]byte, size+len(next))

	copy(temp[0:size], *r.line)
	copy(temp[size:], next)

	r.line = &temp
}

func (r *Reader) consumeLine() (result []byte) {
	result, r.line = *r.line, nil
	return
}

// this just reads a single line of text from the io.Reader.
// returns either a valid line, OR a file error, but never both.
func (r *Reader) rawReadLine() (line []byte, err error) {
	// Note: we use bufio.ReadLine to handle '\r' (carriage returns)
	// in the file, but I'm not sure it handles errors properly.

	if r.eof {
		return nil, io.EOF
	}

	// bump the raw line number
	r.raw_lno += 1

	raw, prefix, err := r.file.ReadLine()
	if err == io.EOF {
		r.eof = true
		return
	}
	if err != nil {
		return
	}

	line = make([]byte, len(raw))
	copy(line, raw)

	for prefix {
		raw, prefix, err = r.file.ReadLine()

		if err == io.EOF {
			r.eof = true
			err = nil
			break
		}
		if err != nil {
			line = nil
			return
		}

		temp := make([]byte, len(line)+len(raw))
		copy(temp[len(line):], raw)

		line = temp
	}

	return
}

func isKeyword(s string) bool {
	runes := []rune(s)

	if len(runes) == 0 {
		return false
	}

	ch := runes[0]

	if !(unicode.IsLetter(ch) || ch == '_') {
		return false
	}

	runes = runes[1:]

	for _, ch = range runes {
		if !(unicode.IsLetter(ch) || unicode.IsDigit(ch) || ch == '_') {
			return false
		}
	}

	return true
}

func isBlankOrComment(line []byte) bool {
	for len(line) > 0 {
		ch, size := utf8.DecodeRune(line)
		if ch == ';' || ch == '#' {
			return true
		}
		if size == 0 || !unicode.IsSpace(ch) {
			return false
		}

		line = line[size:]
	}

	return true
}

func isExtensionLine(line []byte) bool {
	if len(line) == 0 {
		return false
	}

	if isBlankOrComment(line) {
		return false
	}

	ch, size := utf8.DecodeRune(line)
	return size > 0 && unicode.IsSpace(ch)
}

func expectSymbol(buf *bytes.Buffer, ch rune) bool {
	if buf.Len() == 0 {
		return false
	}
	ch2, _, err := buf.ReadRune()
	if err != nil {
		return false
	}
	if ch2 != ch {
		buf.UnreadRune()
		return false
	}
	return true
}

func skipWhitespace(buf *bytes.Buffer) {
	for buf.Len() > 0 {
		ch, _, err := buf.ReadRune()
		if err != nil {
			return
		}
		if !unicode.IsSpace(ch) {
			buf.UnreadRune()
			return
		}
	}
}
