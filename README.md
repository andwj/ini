
Package INI
===========

*A Go package for reading and writing INI-style config files.*

by Andrew Apted, 2017.


Introduction
------------

This is a Go (golang) library for reading and writing
[INI](https://en.wikipedia.org/wiki/INI_file) format config files.

The main goal is to use reflection to make it easy to read and
write whole "records" in the file, where each record is represented
by a Go struct in your program.


Usage
-----

Please visit the [GoDoc page](https://www.godoc.org/gitlab.com/andwj/ini)
for documentation.


Status
------

Version is 0.7.0

The API is fairly complete, and reading and writing are fully
implemented and undergone some light testing.  However there are
likely to be some bugs lurking in there, or edge cases which are
not handled properly.

Also, this is currently lacking examples and unit tests.


License
-------

The license is MIT-style, a permissive open source license.
See the [LICENSE.md](LICENSE.md) file for the complete text.

