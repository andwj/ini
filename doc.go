// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

/*

Package ini supports reading and writing INI-style config files.

by Andrew Apted, 2017.


Introduction

This focus of this package is reading and writing "records" in a
file with a syntax based on INI files and comparable to TOML files.
Here is a basic example of the syntax:

	; this is a comment
	[RECORD_NAME]
	field_name = 12345
	another_field = "a string value"

One goal is that multiple types of records will be supported in a
single file.  Different record types can distinguished by their name,
or a certain prefix of the name.  For example, the "PERSON/" prefix
could be used for records about people and the "COMPANY/" prefix could
be for records about companies.  If the whole file contains only a
single type of record, then prefixes are not required.

Another goal is the usage of Go reflection to make it easy to read and
write records.  The Write() method only needs a pointer to a struct.
Reading is a not much harder, first you need to register the expected
types and their prefix, and the Read() method will parse that struct
and give you the new object via a callback function.

It is NOT a goal is to support deep type heirarchies.  If you need to
handle records which contain arrays of maps, or maps of large structs,
etc, then this package is probably not for you.  This package aims to
keep the generated INI files as ergonomic as possible for humans to
read and edit, and not overly difficult to write a parser in other
programming languages.


Syntax

The supported syntax is roughly the INI format, as follows:

	- blank lines are ignored
	- lines beginning with a semicolon (';') are comments and are ignored
	- records begin with a word in between square brackets ('[' and ']')
	- fields are specified by a keyword followed by '=' and the value

One important addition to the format: lines beginning with whitespace
and which are not completely blank are considered to be an EXTENSION of
the previous line.  This allows large arrays, maps (etc) to be used
without causing excessively long lines in the output file.  Hence record
names in [] and field names MUST occur at the beginning of a line.

Comment lines can also begin with the hash symbol ('#').  Either can be
produced by the writer, and both are accepted by the reader, so which
one is used is a matter of personal taste.

By convention, record names are uppercase and field names are lowercase.
The writer will enforce this convention, but the reader will compare
names in a case-insensitive way.  E.g. "FOO", "Foo" and "foo" will be
considered the same field name.


Field Types

Integers and floating point values are handled as you would expect.
Complex numbers are formatted in '(' and ')' parentheses, as produced
by the Go fmt package.

Booleans are written as uppercase: TRUE, FALSE.  When parsed, the case
is not significant.

Basic type example:
	[HOUSE_1]
	door_num = 42
	height = 19.34
	complex_value = (1.0+2.5i)
	has_fireplace = TRUE

Strings generally use double quotes, and unprintable characters
(including a double quote itself) are escaped with a backslash '\'.
This matches the behavior of the "%q" format string.
For top-level fields, overly long strings will be split into multiple
strings and placed on separate lines, with a plus sign ('+') added to
signal concatenation.  Strings nested in other types are never split,
and hence should be kept fairly short.

Strings which resemble keywords are written without quotes.  This can
be useful for values which are "enum-like" and have several named
alternatives.  It is recommended that keywords be uppercase.

String example:
	short = "a winter's day"
	longer = "Let Hercules himself do what he may.\n" +
	         "The cat will mew, and dog will have his day.\n"
	gender = FEMALE

Arrays are a group of zero or more elements of the same type.
They begin with an open square bracket '[', followed by each element
separated by a comma, and finishing with a close square bracket ']'.
If the line would be overly long, then the writer will split it into
multiple lines.

Arrays need to use the Go slice type; fixed-size arrays are not
supported.  A slice which is nil is written as an empty array: "[]".

Array example:
	short = [9, 7, 3, 4]
	longer = [ agoraphobia, coulrophobia,
	           dentophobia, ichthyophobia,
	           globophobia, ophidiophobia ]

Maps are dictionaries with key/value pairs.  A map begins with an
an open curly brace '{', followed by each key/value pair separated
by a comma, and finishing with a close curly bracke '}'.  Between
the key and the value is a colon ':'.  If a line becomes overly
long, then it is split into multiple lines by the writer.

Map keys must be a basic type or a string.  String keys and values
which resemble identifiers will be written without quotes.
A map whose Go value is nil is written as an empty map: "{}".

Map example:
	short = { foo:3, bar:4 }
	longer = { 15:"Elizabeth Taylor",
	           21:"Gregory Peck",
	           36:"Audrey Hepburn",
	           42:"Marlon Brando" }

Tuples are structs which are formatted inside '(' and ')' parentheses,
where each value is written separated by commas, but the field names
are absent.  The signature of tuples must be registered with a writer,
whereas the reader will handle them automatically.  This syntax is
never split into multiple lines.

Normal structs can also be used for top-level fields, and use a syntax
inside '{' and '}' curly brackets exactly the same as maps (see above).
Field names are always identifiers.  Only exported fields of a Go struct
are read or written, non-exported fields are completely ignored.

Struct example:
	tuple = (123, FALSE, "a white dog")
	normal = { shape:"circle", size:123, pos:(100,200) }

Pointers, functions, channels, fixed-size arrays and interface values
are NOT supported.  They will cause a panic when found.

Composite types generally cannot be nested, such as an array of maps.
However strings are considered to be a basic type, and so are "tuple
structs", hence these CAN be use in composite types.


Usage

For writing, first create an ini.Writer via ini.NewWriter().
There are a few options which can be set, such as using hash '#' for
comment lines.  You may also need to register some tuple via the
RegisterTuples() method.

Individual records can be written using the Write() method.
Alternatively, you can write a whole array of records via the WriteAll()
method.  Be sure to check the returned error.
Comments and blank lines can also be written to the INI file, see the
CommentLine() and BlankLine() methods.

For reading, first create an ini.Reader via ini.NewReader().
This also has a few options, such as ignoring unknown fields.
You then need to register some singleton records via RegisterVar()
and/or some wildcard records via RegisterType().  Singleton records
have an exact name, and the variable is updated directly, whereas
wildcard records will match a certain prefix and construct a new Go
object for each matching record in the INI file.

Use the Read() or ReadAll() methods to process the INI file.
Which one to use depends on how fine-grained you want to handle
parsing errors and unknown record or field names.  ReadAll() is good
when you expect the file to parse successfully, and if not then your
program just aborts.  Use Read() if you want to continue reading after
a parse error, and handle each error fully, e.g. show each one
to the user.  Note that I/O errors (any error not defined by this
package) should be treated as fatal.

When an error occurs, there are some methods to get more information.
For parse errors, the ErrorParseInfo() will return a message about
what failed to parse.  For most errors, you can get the line number,
the name of the record it occurred in, and the name of the field.
This information can be used to construct a useful message for the
user.


License

Copyright (c) 2017 Andrew Apted

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ini
