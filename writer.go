// Copyright 2017 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the accompanying "LICENSE.md" file for the full text.

package ini

import (
	"bufio"
	"fmt"
	"io"
	"reflect"
	"strings"
	"unicode/utf8"
)

// Writer implements writing records to an io.Writer, formatting
// each record in an INI-style syntax.
type Writer struct {
	file  *bufio.Writer
	width int

	comment_char rune
	abbreviate   bool

	tuple_types map[string]bool

	split struct {
		cur_x   int
		align   int
		got_one bool
	}
}

// NewWriter creates a Writer which writes to the given io.Writer.
// This is the only way to create a valid Writer object.  While using
// the Writer, the underlying object (a file, etc) should not be used
// directly.
func NewWriter(file io.Writer) *Writer {
	w := new(Writer)
	w.file = bufio.NewWriter(file)
	w.width = 76
	w.comment_char = ';'
	w.tuple_types = make(map[string]bool)
	return w
}

// SetWidth sets the preferred text width in the output file, which
// controls how long certain lines can be before they are split into
// multiple lines.  Note that this is not strictly enforced, e.g.
// when writing a sub-struct, individual fields are never split.
// The default value is designed for 80-column terminals.
func (w *Writer) SetWidth(width int) {
	if width < 8 {
		panic("width for ini.Writer is too small")
	}

	w.width = width
}

// EnableHashComments changes the character used for comment lines.
// The default is a semicolon (';').  This method will switch the
// comment character to the hash symbol ('#').  Note that both forms
// are accepted by the parser.
func (w *Writer) EnableHashComments() {
	w.comment_char = '#'
}

// EnableAbbreviate makes the writer skip fields that have default
// values.  This will skip zero integers and floats, FALSE booleans,
// nil or empty slices and maps, etc....  Sub-structs will be
// affected too, but not the contents of arrays, slices or maps.
// The default behavior is to write all exported fields of a struct.
func (w *Writer) EnableAbbreviate() {
	w.abbreviate = true
}

// WriteAll takes a slice of pointers to structs, and writes each
// struct into the INI file via the Write() method.  The structs
// must all be the same type.
//
// The record name in [] will be the given 'prefix' string plus a
// computed suffix.  If the structs have an "Id" field, that is used
// as the suffix, otherwise it is a number based on the index in the
// slice.  These numeric suffixes begin at 1 (not zero).
//
// If a file error occurs, it is returned immediately and you must
// assume the output file is corrupt and useless (ideally delete it).
// On success, nil is returned.
func (w *Writer) WriteAll(ptrs interface{}, prefix string) error {
	if ptrs == nil {
		panic("nil value given to ini.WriteAll")
	}

	arr := reflect.ValueOf(ptrs)

	if arr.Kind() != reflect.Slice {
		panic("non-slice given to ini.WriteAll")
	}

	arrLen := arr.Len()

	for i := 0; i < arrLen; i++ {
		elem := arr.Index(i)

		rec_ptr := elem.Addr()

		name := w.nameForElement(i, elem, prefix)

		err := w.Write(rec_ptr.Interface(), name)
		if err != nil {
			return err
		}
	}

	return nil
}

func (w *Writer) nameForElement(index int, elem reflect.Value,
	prefix string) string {

	id := elem.FieldByName("Id")

	if id.IsValid() {
		if id.String() == "" {
			panic("unset Id value in record")
		}

		return fmt.Sprintf("%s%s", prefix, id.String())
	}

	return fmt.Sprintf("%s%d", prefix, index+1)
}

// Write writes a single record to the INI file, using our INI-like
// syntax.  The caller must ensure the name is appropriate for the
// type of this record (e.g. has the correct prefix).  The name is
// converted to uppercase.
//
// The 'rec_ptr' parameter should be a pointer to a Go struct.  All
// the exported fields of this struct will be written to the file.
// Field names are converted to lowercase.
//
// Note: unsupported types in the struct will cause a panic.
//
// If a file error occurs, it is returned immediately and you must
// assume the output file is corrupt and useless (ideally delete it).
// On success, nil is returned.
func (w *Writer) Write(rec_ptr interface{}, name string) error {
	if rec_ptr == nil {
		panic("nil value given to ini.Write")
	}

	ptr := reflect.ValueOf(rec_ptr)

	if ptr.Type() == nil || ptr.Kind() != reflect.Ptr {
		panic("non-pointer value given to ini.Write")
	} else if ptr.IsNil() {
		panic("nil pointer given to ini.Write")
	}

	rec := ptr.Elem()

	if rec.Kind() != reflect.Struct {
		panic("pointer to non-struct given to ini.Write")
	}

	// add the record header
	_, err := fmt.Fprintf(w.file, "[%s]\n", strings.ToUpper(name))
	if err != nil {
		return err
	}

	// add the fields
	for i := 0; i < rec.NumField(); i++ {
		if err := w.writeField(rec, i); err != nil {
			return err
		}
	}

	// finish with a blank line
	// Note: this also does a Flush() for us
	return w.BlankLine()
}

func (w *Writer) writeField(rec reflect.Value, index int) error {
	field := rec.Field(index)
	info := rec.Type().Field(index)
	kind := field.Kind()

	// ignore non-exported fields
	if info.PkgPath != "" {
		return nil
	}

	// ignore a field called "Id"
	if info.Name == "Id" {
		return nil
	}

	if w.abbreviate && w.isUnset(field) {
		return nil
	}

	// begin the new field
	name := strings.ToLower(info.Name)

	_, err := fmt.Fprintf(w.file, "%s = ", name)
	if err != nil {
		return err
	}

	// handle "primitive" types
	if kind < reflect.Array {
		s := w.formatValue(field)
		_, err = fmt.Fprintf(w.file, "%s\n", s)
		return err
	}

	if w.isTupleStruct(info.Type) {
		s := w.formatTuple(field)
		_, err = fmt.Fprintf(w.file, "%s\n", s)
		return err
	}

	w.split.cur_x = utf8.RuneCountInString(name) + 3
	w.split.align = w.split.cur_x
	w.split.got_one = false

	switch kind {
	case reflect.String:
		return w.writeString(field.String())

	case reflect.Slice:
		if field.IsNil() {
			return w.rawWrite("[]\n")
		} else {
			return w.writeArray(field)
		}

	case reflect.Struct:
		return w.writeStruct(field)

	case reflect.Map:
		return w.writeMap(field)

	default:
		panic("unsupported field type: " + kind.String())
	}
}

func (w *Writer) writeString(s string) error {
	s = fmt.Sprintf("%q", s)

	runes := []rune(s)

	// remove the double quotes
	if len(runes) >= 2 {
		runes = runes[1 : len(runes)-1]
	}

	space := w.width - w.split.align
	if space < w.width/2 {
		space = w.width / 2
	}

	num_lines := 1 + len(runes)/(space-4)
	chunk_len := 1 + len(runes)/num_lines

	for i := 0; i < num_lines; i++ {
		use_len := chunk_len
		if use_len > len(runes) || i == (num_lines-1) {
			use_len = len(runes)
		}

		if err := w.rawWrite("\""); err != nil {
			return err
		}

		for _, r := range runes[0:use_len] {
			if _, err := fmt.Fprintf(w.file, "%c", r); err != nil {
				return err
			}
		}

		if err := w.rawWrite("\""); err != nil {
			return err
		}

		if i < num_lines-1 {
			if err := w.rawWrite(" +"); err != nil {
				return err
			}
			if err := w.rawBeginLine(); err != nil {
				return err
			}
		}

		runes = runes[use_len:]
	}

	return w.rawWrite("\n")
}

func (w *Writer) writeArray(arr reflect.Value) error {
	if arr.Len() == 0 {
		return w.rawWrite("[]\n")
	}

	if err := w.rawWrite("[ "); err != nil {
		return err
	}

	w.split.align = w.split.cur_x

	for i := 0; i < arr.Len(); i++ {
		elem := w.formatValue(arr.Index(i))

		if err := w.writeNextValue(elem); err != nil {
			return err
		}
	}

	return w.rawWrite(" ]\n")
}

func (w *Writer) writeStruct(obj reflect.Value) error {
	if err := w.rawWrite("{ "); err != nil {
		return err
	}

	w.split.align = w.split.cur_x

	tp := obj.Type()

	for i := 0; i < obj.NumField(); i++ {
		field := obj.Field(i)
		info := tp.Field(i)

		// ignore non-exported fields
		if info.PkgPath != "" {
			continue
		}

		if w.abbreviate && w.isUnset(field) {
			return nil
		}

		s := fmt.Sprintf("%s:%s", info.Name, w.formatValue(field))

		if err := w.writeNextValue(s); err != nil {
			return err
		}
	}

	return w.rawWrite(" }\n")
}

func (w *Writer) writeMap(obj reflect.Value) error {
	if obj.IsNil() || obj.Len() == 0 {
		return w.rawWrite("{}\n")
	}

	if err := w.rawWrite("{ "); err != nil {
		return err
	}

	w.split.align = w.split.cur_x

	for _, k := range obj.MapKeys() {
		val := obj.MapIndex(k)

		// this probably cannot happen, check anyway
		if !val.IsValid() {
			continue
		}

		s := fmt.Sprintf("%s:%s", w.formatValue(k), w.formatValue(val))

		if err := w.writeNextValue(s); err != nil {
			return err
		}
	}

	return w.rawWrite(" }\n")
}

func (w *Writer) writeNextValue(s string) error {
	if !w.split.got_one {
		w.split.got_one = true
	} else {
		if err := w.rawWrite(", "); err != nil {
			return err
		}

		count := utf8.RuneCountInString(s)

		// need a fresh line?
		if w.split.cur_x+count >= w.width {
			if err := w.rawBeginLine(); err != nil {
				return err
			}
		}
	}

	return w.rawWrite(s)
}

func (w *Writer) rawWrite(s string) error {
	w.split.cur_x += utf8.RuneCountInString(s)

	_, err := io.WriteString(w.file, s)
	return err
}

func (w *Writer) rawBeginLine() error {
	w.split.cur_x = w.split.align

	t := "\n" + strings.Repeat(" ", w.split.align)
	_, err := io.WriteString(w.file, t)
	return err
}

func (w *Writer) formatValue(val reflect.Value) string {
	tp := val.Type()

	kind := val.Kind()

	if kind == reflect.Bool {
		if val.Bool() {
			return "TRUE"
		} else {
			return "FALSE"
		}
	}

	// for "primitive" types, just use the fmt package
	if kind < reflect.Array {
		return fmt.Sprintf("%v", val.Interface())
	}

	if kind == reflect.String {
		s := val.String()

		if isKeyword(s) {
			return s
		} else {
			return fmt.Sprintf("%q", s)
		}
	}

	if w.isTupleStruct(tp) {
		return w.formatTuple(val)
	}

	panic("composite types cannot be nested")
}

func (w *Writer) isUnset(val reflect.Value) bool {
	switch val.Kind() {
	case reflect.Bool:
		return val.Bool() == false
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return val.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return val.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return val.Float() == 0.0
	case reflect.Complex64, reflect.Complex128:
		return val.Complex() == (0 + 0i)
	case reflect.Slice, reflect.Map:
		return val.IsNil() || val.Len() == 0
	case reflect.String:
		return val.String() == ""
	default:
		return false
	}
}

func (w *Writer) isTupleStruct(tp reflect.Type) bool {
	if tp.Kind() != reflect.Struct {
		return false
	}

	for sig, _ := range w.tuple_types {
		if matchTupleSignature(tp, sig) {
			return true
		}
	}

	return false
}

func (w *Writer) formatTuple(val reflect.Value) string {
	tp := val.Type()

	s := "("

	got_one := false

	for i := 0; i < tp.NumField(); i++ {
		field := val.Field(i)
		info := tp.Field(i)

		// ignore non-exported fields
		if info.PkgPath != "" {
			continue
		}

		if got_one {
			s += ", "
		}

		s += w.formatValue(field)

		got_one = true
	}

	s += ")"

	return s
}

// CommentLine writes a single comment line to the file.  This can
// be used for top-of-file comments, or a comment before a particular
// record in the INI file.  The given string should NOT be prefixed
// by the comment character or contain any newlines.
func (w *Writer) CommentLine(line string) error {
	_, err := fmt.Fprintf(w.file, "%c %s\n", w.comment_char, line)
	if err != nil {
		return err
	}

	return w.file.Flush()
}

// BlankLine writes a blank line to the file.  This is mainly useful
// to separate some top-of-file comments from the rest of the file.
func (w *Writer) BlankLine() error {
	if _, err := fmt.Fprintln(w.file); err != nil {
		return err
	}

	return w.file.Flush()
}

// RegisterTuples marks one or more struct types that will be treated
// as a "tuple".  These structs can be nested in other complex types,
// like arrays and maps, and are written in a compact notation: a
// comma-separated group of values in '(' ')' parentheses.
//
// Each parameter is a string representing the type signature of the
// tuple struct.  The string can be constructed by visiting each
// exported field of the struct, and adding a character for the type
// of that field, as follows:
//
//    b = boolean
//    i = any integer type
//    f = any float type
//    c = any complex type
//    s = string
//
// If the struct contains any other type (in an exported field), then
// it cannot be used as a tuple struct.
//
// For example: "fff" can be used to represent a 3D coordinate, and
// "iiii" can be used for the RGBA type in the image/color package.
func (w *Writer) RegisterTuples(sigs ...string) {
	for _, s := range sigs {
		if s == "" {
			panic("empty signature given to ini.RegisterTuples")
		}

		w.tuple_types[s] = true
	}
}

func matchTupleSignature(tp reflect.Type, sig string) bool {
	pos := 0

	for i := 0; i < tp.NumField(); i++ {
		info := tp.Field(i)

		// ignore non-exported fields
		if info.PkgPath != "" {
			continue
		}

		// too many fields?
		if pos >= len(sig) {
			return false
		}

		var ch byte

		switch info.Type.Kind() {
		case reflect.Bool:
			ch = 'b'
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			ch = 'i'
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			ch = 'i'
		case reflect.Float32, reflect.Float64:
			ch = 'f'
		case reflect.Complex64, reflect.Complex128:
			ch = 'c'
		case reflect.String:
			ch = 's'
		default:
			return false
		}

		if sig[pos] != ch {
			return false
		}

		pos++
	}

	return pos == len(sig)
}
